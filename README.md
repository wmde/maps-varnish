Running
===

  docker build -t maps-varnish .

  docker run --rm -p 6534:80/tcp --name varnish-container --tmpfs /var/lib/varnish:exec maps-varnish -p vsl_mask=+Hash

  docker exec -it varnish-container varnishlog | grep -i hash

  wget 'http://localhost:6534?domain=abc&foo=1'

Expected results are like,

```
  -   VCL_return     hash
  -   VCL_call       HASH
  -   Hash           "/%00"
  -   Hash           "domain=abc%00"
  -   Hash           "localhost:6534%00"
```

and for a maximal example,

  wget 'http://localhost:6534/123?domain=abc&foo=1&groups=2&lang=de&bar=4'

```
  -   VCL_return     hash
  -   VCL_call       HASH
  -   Hash           "/123%00"
  -   Hash           "domain=abc%00"
  -   Hash           "groups=2%00"
  -   Hash           "lang=de%00"
  -   Hash           "localhost:6534%00"
```
