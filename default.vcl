vcl 4.0;

backend default {
  # TODO: Use the internal vs external port depending on env.
  .host = "localhost:6533";
}

sub vcl_hash {
  # URL without query parameters
  hash_data(regsub(req.url, "^(.+)\?.*$", "\1"));
  # Add specific parameters back in again.
  if (req.url ~ "[?&]domain=") {
    hash_data(regsub(req.url, "^.*[?&](domain=[^&]*).*$", "\1"));
  }
  if (req.url ~ "[?&]groups=") {
    hash_data(regsub(req.url, "^.*[?&](groups=[^&]*).*$", "\1"));
  }
  if (req.url ~ "[?&]lang=") {
    hash_data(regsub(req.url, "^.*[?&](lang=[^&]*).*$", "\1"));
  }
  # Add host clause from the builtin rules.
  if (req.http.host) {
    hash_data(req.http.host);
  } else {
    hash_data(server.ip);
  }
  return (lookup);
}
